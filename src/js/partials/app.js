$(document).ready(function (){

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};

	$('body').removeClass('loaded'); 

	/* placeholder*/	   
	$('input, textarea').each(function () {
		var placeholder = $(this).attr('placeholder');

		$(this).focus(function () { 
			$(this).attr('placeholder', '');
		});

		$(this).focusout(function () {			 
			$(this).attr('placeholder', placeholder);  			
		});
	});
	/* placeholder */

	/* nav__item active */
	$('.nav__link').click(function () {
		$('.nav__item.active').removeClass('active');
		$(this).parent().addClass('active');
	});
	/* nav__item active */

	/* slock-slider */
	$('.slik-slider').slick({
		dots: true,
		autoplay: true,
		prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left"></i></div>',
		nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></div>',
		infinite: true,
		speed: 400
	});

});

/* viewport width */
function viewport () {
	var e = window; 
	var	a = 'inner';

	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { 
		width : e[ a+'Width' ],
		height : e[ a+'Height' ] 
	};
};
/* viewport width */

var handler = function () {

	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;

};

$(window).bind('load', handler);
$(window).bind('resize', handler);



